const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const PROD = false;


const config = {
    entry: [
        './src/main.js'
    ],
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'web/built')
    },

    // Configure how modules are resolved. For example, when calling import "lodash" in ES2015,
    // the resolve options can change where webpack goes to look for "lodash"
    resolve: {
        extensions: ['.js', '.vue'],
        alias: {
            'vue': 'vue/dist/vue.common.js' // to solve this problem You are using the runtime-only build of Vue where the template option is not available.
        }
    },

    devServer: {
        hot: true,
        inline: true,
        open: true,
        contentBase: './web/'
    },

    module: {
        loaders: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({use: 'css-loader', fallback: 'style-loader'})
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)(\?\S*)?$/,
                loader: 'file-loader'
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?\S*)?$/,
                loader: 'file-loader',
                query: {
                    name: '[name].[ext]?[hash]'
                }
            }
        ]
    },
    plugins: PROD === true ? [
        new webpack.optimize.UglifyJsPlugin({
            compress: { warnings: false }
        })
    ] : [
        new webpack.HotModuleReplacementPlugin(),
        new ExtractTextPlugin('main.css')
    ]
};

module.exports = config;


