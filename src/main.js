/**
 * Created by lukas on 5/21/17.
 */
import Vue from 'vue';
import App from './App.vue'
import KeenUI from 'keen-ui';


import 'normalize.css/normalize.css'
import 'flexboxgrid/dist/flexboxgrid.min.css'
import 'keen-ui/dist/keen-ui.min.css'

Vue.use(KeenUI);


new Vue({
    el: '#app',
    render: h => h(App)
});



